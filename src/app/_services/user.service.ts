import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models';
import {REGISTRATION_V1_API} from '../_helpers/api-constants';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(REGISTRATION_V1_API("users"));
    }

    getById(id: number) {
        return this.http.get<User>(REGISTRATION_V1_API("users/" + id));
    }

    getByEmail(email: number) {
        return this.http.get<User>(REGISTRATION_V1_API("users/email/" + email));
    }

    register(user: User) {
        return this.http.post<User>(REGISTRATION_V1_API("auth/register"), user);
    }

    update(user: User) {
        return this.http.patch<User>(REGISTRATION_V1_API("users/" + user.id), user);
    }

    changeRole(userId: number, roleId: number) {
        return this.http.patch(REGISTRATION_V1_API("users/" + userId + "/role"), {
            userId: userId,
            roleId: roleId,
        })
    }
}