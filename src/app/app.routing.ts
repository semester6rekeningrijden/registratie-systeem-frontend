import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/home';
import { AuthGuard } from './_guards';

const appRoutes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'auth', loadChildren: './pages/auth/auth.module#AuthModule' },
    { path: 'users', loadChildren: './pages/users/users.module#UsersModule', canActivate: [AuthGuard] },
    { path: 'roles', loadChildren: './pages/roles/roles.module#RolesModule', canActivate: [AuthGuard] },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);