import { Component, OnInit } from '@angular/core';
import {CountryService, UserService} from '../../../_services';
import {AlertService} from '../../../_services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../../_models';
import {Observable} from 'rxjs/internal/Observable';
import {Role} from '../../../_models/role';
import {RoleService} from '../../../_services/role.service';

@Component({
  selector: 'app-updateusers',
  templateUrl: './createroles.component.html',
  styleUrls: ['./createroles.component.css']
})
export class CreaterolesComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;

  constructor(
      private activatedRoute: ActivatedRoute,
      private formBuilder: FormBuilder,
      private router: Router,
      private roleService: RoleService,
      private alertService: AlertService
  ) { }

  ngOnInit() {
      this.registerForm = this.formBuilder.group({
          name: ['', Validators.required],
      });
  }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;

        if(this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        this.roleService.create(this.registerForm.value).forEach((data) => {
            this.alertService.success('Role Created', true);
            this.router.navigate(['/roles']);
        }).catch((err) => {
            this.alertService.error(err.error);
            this.loading = false;
        });
    }

}
