import { Component, OnInit } from '@angular/core';
import {AlertService} from '../../../_services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs/internal/Observable';
import {Role} from '../../../_models/role';
import {RoleService} from '../../../_services/role.service';
import {Permission} from '../../../_models/permission';
import {PermissionsService} from '../../../_services/permissions.service';

@Component({
  selector: 'app-updateusers',
  templateUrl: './attachpermissions.component.html',
  styleUrls: ['./attachpermissions.component.css']
})
export class AttachpermissionsComponent implements OnInit {
    attachForm: FormGroup;
    detachForm: FormGroup;
    loading = false;
    submitted = false;

    private roleId: number;
    private role: Observable<Role>;
    private permissions: Observable<Permission[]>

  constructor(
      private activatedRoute: ActivatedRoute,
      private formBuilder: FormBuilder,
      private router: Router,
      private roleService: RoleService,
      private permissionService: PermissionsService,
      private alertService: AlertService
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.forEach((params) => {
      this.roleId = parseInt(params.get("id"));
    }).catch((error) => {
      this.alertService.error(error.error);
      this.router.navigate(["/roles"]);
    });

    this.role = this.roleService.getById(this.roleId);
    this.permissions = this.permissionService.getAll();

      this.attachForm = this.formBuilder.group({
          roleId: ['', Validators.required],
          permissionId: ['', Validators.required],
      });

      this.detachForm = this.formBuilder.group({
          roleId: ['', Validators.required],
          permissionId: ['', Validators.required],
      });

      this.role.forEach((role) => {
          this.attachForm = this.formBuilder.group({
              roleId: [role.id, Validators.required],
              permissionId: ['', Validators.required],
          });

          this.detachForm = this.formBuilder.group({
              roleId: [role.id, Validators.required],
              permissionId: ['', Validators.required],
          });
      }).catch((error) => {
          this.alertService.error(error.error);
          this.router.navigate(["/roles"]);
      });
  }

    // convenience getter for easy access to form fields
    get a() { return this.attachForm.controls; }

    // convenience getter for easy access to form fields
    get d() { return this.detachForm.controls; }

    attach() {
        this.submitted = true;

        if(this.attachForm.invalid) {
            return;
        }

        this.loading = true;
        this.roleService.attach(this.roleId, this.a.permissionId.value).forEach((data) => {
            console.log("DONE:");
            this.alertService.success('Permission Attached', true);
            this.router.navigate(['/roles']);
        }).catch((err) => {
            console.log(JSON.stringify(err));
            this.alertService.error(err.error);
            this.loading = false;
        });
    }

    detach() {
        this.submitted = true;

        if(this.detachForm.invalid) {
            return;
        }

        this.loading = true;
        this.roleService.detach(this.d.roleId.value, this.d.permissionId.value).forEach((data) => {
            this.alertService.success('Permission Detached', true);
            this.router.navigate(['/roles']);
        }).catch((err) => {
            this.alertService.error(err.error);
            this.loading = false;
        });
    }

}
